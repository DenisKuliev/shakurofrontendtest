# Refactoring

Refactoring result is in refactoring.ts file in root o this repo.

# Practical Test
## Tech used
- Angular 9 - as robust type-safe TypeScript framework for SPA apps
- Angular Material 9 - for material design components
- @angular/flex-layout - for wrapping css flex for more declarative flexbox usage
- angular2-text-mask - for masking phone and payment amount fields

## Working application
Located at https://zen-galileo-63d078.netlify.com