function lastIndexOfChar(haystack: string, ...needles: string[]) {

  for(let i = haystack.length - 1; i >= 0; i--)
    if (needles.some(char => char == haystack[i]))
      return i;

  return -1;
}
