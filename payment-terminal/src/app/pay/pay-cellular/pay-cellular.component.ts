import { Component, OnInit } from '@angular/core';
import { CellOperator } from '../operators/operator/cell-operator';
import { ActivatedRoute } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';
import { OperatorsService } from '../operators/operators.service';
import { Payment } from './payment';

@Component({
  selector: 'app-pay-cellular',
  templateUrl: './pay-cellular.component.html'
})
export class PayCellularComponent implements OnInit {

  operator$: Promise<CellOperator>;

  payment: Payment = {operatorId: '', amount: 0, number: ''};

  countryCode = '+7';

  phoneNumberFormGroup: FormGroup;
  paymentAmountFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private operatorsService: OperatorsService) {
  }

  ngOnInit() {
    this.operator$ = this.operatorsService.get().pipe(
      map(cellOperators => cellOperators
        .find(operator => operator.id == this.activatedRoute.snapshot.queryParamMap.get('id'))),
      tap(operator => this.payment = {operatorId: operator.id, amount: 0, number: ''})
    ).toPromise();

    this.phoneNumberFormGroup = this.formBuilder.group({});

    this.paymentAmountFormGroup = this.formBuilder.group({});
  }

  onPhoneSubmit() {
    const payment = this.payment;
    const number = `${this.countryCode} ${this.phoneNumberFormGroup.controls.phone.value}`;

    this.payment = {...payment, number};
  }

  onAmountSubmit() {
    const payment = this.payment;
    const amount = this.paymentAmountFormGroup.controls.paymentAmount.value;

    this.payment = {...payment, amount};
  }
}
