import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { CellOperator } from '../../operators/operator/cell-operator';
import { maskNumberLengthValidator } from '../maskNumberLengthValidator';

@Component({
  selector: 'app-phone-number-input',
  templateUrl: './phone-number-input.component.html',
  styleUrls: ['./phone-number-input.component.scss']
})
export class PhoneNumberInputComponent implements OnInit {

  @Input()
  operator: CellOperator;

  @Input()
  countryCode: string;

  formGroup: FormGroup;

  phoneNumberMask = ['(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

  constructor(private formGroupDirective: FormGroupDirective, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.formGroup = this.formGroupDirective.control;

    this.formGroup.addControl('phone', this.formBuilder.control('', [Validators.required,
      maskNumberLengthValidator(10)]))
  }
}
