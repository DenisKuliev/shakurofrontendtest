import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';

@Component({
  selector: 'app-amount-input',
  templateUrl: './amount-input.component.html'
})
export class AmountInputComponent implements OnInit {

  formGroup: FormGroup;

  paymentAmountMask = [/\d/, /\d/, /\d/, /\d/];

  constructor(private formGroupDirective: FormGroupDirective, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.formGroup = this.formGroupDirective.control;

    this.formGroup.addControl('paymentAmount', this.formBuilder.control('',
      [Validators.required, Validators.min(1), Validators.max(1000)]));
  }
}
