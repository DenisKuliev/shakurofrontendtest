import { Component, Input, OnInit } from '@angular/core';
import { Payment } from '../payment';
import { PaymentService } from '../payment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html'
})
export class PayComponent implements OnInit {

  @Input()
  payment: Payment;

  isLoading = false;

  constructor(private paymentService: PaymentService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  async pay() {
    this.isLoading = true;
    const paymentId = await this.paymentService.pay(this.payment).toPromise();

    if (paymentId !== undefined) {
      await this.router.navigate(['/success'], {queryParams: {id: paymentId}});

      return;
    }

    await this.router.navigate(['/failed']);
  }
}
