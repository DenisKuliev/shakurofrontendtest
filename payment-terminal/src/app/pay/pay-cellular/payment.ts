export interface Payment {
  operatorId: string;
  amount: number;
  number: string;
}
