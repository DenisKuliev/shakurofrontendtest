import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export const onlyDigitsRegExp = /[^\d+]/g;

export function maskNumberLengthValidator(length: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {

    const value = control.value.toString().replace(onlyDigitsRegExp, '');

    if(!control.dirty)
      return null;

    if(value.length == length){
      control.setErrors(null);
      return null;
    }

    const validationErrors: ValidationErrors = { invalidLength: true };

    control.setErrors(validationErrors);

    return validationErrors;
  };
}
