import { Injectable } from '@angular/core';
import { Payment } from './payment';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor() { }

  pay(payment: Payment): Observable<number | undefined>{
    const number = Math.random();
    if (number > 0.5) {
      return of(Math.round(number * 1000)).pipe(delay(1000));
    }

    return of(undefined).pipe(delay(1000));
  }
}
