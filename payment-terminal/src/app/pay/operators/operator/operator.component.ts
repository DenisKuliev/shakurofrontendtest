import { Component, Input, OnInit } from '@angular/core';
import { CellOperator } from './cell-operator';

@Component({
  selector: 'app-operator',
  templateUrl: './operator.component.html',
  styleUrls: ['./operator.component.scss']
})
export class OperatorComponent{

  @Input()
  cellOperator: CellOperator;
}
