export interface CellOperator {
  id: string,
  name: string;
  imageUrl: string;
}
