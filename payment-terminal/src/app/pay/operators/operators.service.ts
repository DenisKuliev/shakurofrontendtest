import { Injectable } from '@angular/core';
import { CellOperator } from './operator/cell-operator';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OperatorsService {

  constructor() {
  }

  basePath = 'assets/cell-operators'

  get = (): Observable<CellOperator[]> =>
    of<CellOperator[]>([
      {id: '0', name: 'MTS', imageUrl: `${this.basePath}/mts.png`},
      {id: '1', name: 'Beeline', imageUrl: `${this.basePath}/beeline.png`},
      {id: '2', name: 'MegaFone', imageUrl: `${this.basePath}/megaphone.png`}
    ]).pipe(delay(1000));
}
