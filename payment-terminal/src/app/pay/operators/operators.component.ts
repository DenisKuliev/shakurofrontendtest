import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { CellOperator } from './operator/cell-operator';
import { OperatorsService } from './operators.service';

@Component({
  selector: 'app-operators',
  templateUrl: './operators.component.html'
})
export class OperatorsComponent {

  operators$: Observable<CellOperator[]>

  constructor(private operatorsService: OperatorsService) {
    this.operators$ = operatorsService.get();
  }
}
