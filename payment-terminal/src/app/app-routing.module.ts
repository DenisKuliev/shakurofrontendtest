import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TerminalComponent } from './pay/terminal.component';
import { PayCellularComponent } from './pay/pay-cellular/pay-cellular.component';
import { SuccessComponent } from './pay/success/success.component';
import { FailedComponent } from './pay/failed/failed.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: TerminalComponent},
  {path: 'pay-cellular', component: PayCellularComponent},
  {path: 'success', component: SuccessComponent},
  {path: 'failed', component: FailedComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
