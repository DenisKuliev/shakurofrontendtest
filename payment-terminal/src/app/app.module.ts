import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TerminalComponent } from './pay/terminal.component';
import { OperatorComponent } from './pay/operators/operator/operator.component';
import { MatCardModule } from '@angular/material/card';
import { LoadingComponent } from './loading/loading.component';
import { MatRippleModule } from '@angular/material/core';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { OperatorsComponent } from './pay/operators/operators.component';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { MatTabsModule } from '@angular/material/tabs';
import { PayCellularComponent } from './pay/pay-cellular/pay-cellular.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { TextMaskModule } from 'angular2-text-mask';
import { SuccessComponent } from './pay/success/success.component';
import { FailedComponent } from './pay/failed/failed.component';
import { PhoneNumberInputComponent } from './pay/pay-cellular/phone-number-input/phone-number-input.component';
import { AmountInputComponent } from './pay/pay-cellular/amount-input/amount-input.component';
import { PayComponent } from './pay/pay-cellular/pay/pay.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent,
    TerminalComponent,
    OperatorComponent,
    LoadingComponent,
    OperatorsComponent,
    NavBarComponent,
    PayCellularComponent,
    PayComponent,
    SuccessComponent,
    FailedComponent,
    PhoneNumberInputComponent,
    AmountInputComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FlexLayoutModule,
    TextMaskModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatRippleModule,
    MatStepperModule,
    MatInputModule,
    MatButtonModule,
    MatTabsModule,
    MatIconModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
